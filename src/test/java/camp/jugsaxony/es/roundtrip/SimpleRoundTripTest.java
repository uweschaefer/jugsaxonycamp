package camp.jugsaxony.es.roundtrip;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Set;
import java.util.UUID;

import org.factcast.core.FactCast;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import camp.jugsaxony.es.event.cart.LineItemAdded;
import camp.jugsaxony.es.event.cart.LineItemQuantityChanged;
import camp.jugsaxony.es.event.cart.LineItemRemoved;
import camp.jugsaxony.es.model.cart.Cart;
import camp.jugsaxony.es.model.cart.LineItem;
import camp.jugsaxony.es.model.repo.CartRepository;
import camp.jugsaxony.es.model.report.RecommendedArticles;
import camp.jugsaxony.es.service.CartService;
import camp.jugsaxony.es.util.Util;
import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@Slf4j
@SpringBootTest
public class SimpleRoundTripTest {

    @Autowired
    FactCast eventStore;

    @Autowired
    CartRepository repo;

    @Autowired
    RecommendedArticles recommended;

    @Autowired
    CartService cartService;

    @Test
    public void testSimpleRoundtrip() throws Exception {

        System.err.println("es: " + eventStore);

        UUID tshirtId = UUID.fromString("A0000000-0000-0000-0000-000000000001");
        UUID socksId = UUID.fromString("A0000000-0000-0000-0000-000000000002");
        UUID hatId = UUID.fromString("A0000000-0000-0000-0000-000000000003");
        UUID cartId = UUID.fromString("5C000000-0000-0000-0000-000000000001");
        UUID customerId = UUID.fromString("C0000000-0000-0000-0000-000000000001");

        LineItemAdded twoShirtsAdded = new LineItemAdded()
                .articleId(tshirtId)
                .cartId(cartId)
                .customerId(customerId)
                .quantity(2);

        LineItemQuantityChanged shirtQuantityUpdated = new LineItemQuantityChanged()
                .articleId(tshirtId)
                .cartId(cartId)
                .customerId(customerId)
                .quantity(1);

        LineItemAdded socksAdded = new LineItemAdded()
                .articleId(socksId)
                .cartId(cartId)
                .customerId(customerId);

        LineItemRemoved socksRemoved = new LineItemRemoved()
                .articleId(socksId)
                .cartId(cartId)
                .customerId(customerId);

        eventStore.publish(Util.toFact(twoShirtsAdded));
        eventStore.publish(Util.toFact(shirtQuantityUpdated));
        eventStore.publish(Util.toFact(socksAdded));
        eventStore.publish(Util.toFact(socksRemoved));

        Cart cart = repo.findByCartId(cartId);
        assertEquals(1, cart.getLineItems().size());

        LineItem lineItem = cart.getLineItems().values().iterator().next();
        assertEquals(1, lineItem.getQuantity());
        assertEquals(tshirtId, lineItem.getArticleId());

        // virtually free
        cart = repo.findByCartId(cartId);
        cart = repo.findByCartId(cartId);
        cart = repo.findByCartId(cartId);

        // re-adding socks
        eventStore.publish(Util.toFact(new LineItemAdded()
                .articleId(socksId)
                .cartId(cartId)
                .customerId(customerId)
                ));

        log.info("now catchup");

        cart = repo.findByCartId(cartId);
        assertEquals(2, cart.getLineItems().size());

        cartService.addToCart(cartId, hatId, customerId, 1);
        cartService.removeFromCart(cartId, hatId, customerId);
        cartService.checkout(cartId);

        Thread.sleep(1000);

        Set<UUID> recommendedArticles = recommended.getRecommendationsForCustomer(customerId);
        assertEquals(1, recommendedArticles.size());
        assertTrue(recommendedArticles.contains(hatId));

    }

}
