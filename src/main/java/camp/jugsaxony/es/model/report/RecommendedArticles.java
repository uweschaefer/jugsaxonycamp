package camp.jugsaxony.es.model.report;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.factcast.core.FactCast;
import org.factcast.core.spec.FactSpec;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets;

import camp.jugsaxony.es.event.PushReadModel;
import camp.jugsaxony.es.event.cart.CheckoutStarted;
import camp.jugsaxony.es.event.cart.LineItemRemoved;
import camp.jugsaxony.es.model.cart.LineItem;

@Service
public class RecommendedArticles extends PushReadModel {

    final Map<UUID, Set<UUID>> recommendedArticlesByCustomer = new ConcurrentHashMap<>();

    public RecommendedArticles(FactCast eventStore) {
        super(eventStore, () -> FactSpec.ns("jugsaxony"));
    }

    public void apply(LineItemRemoved event) {
        Set<UUID> list = getForCustomer(event.customerId());
        list.add(event.articleId());
    }

    public void apply(CheckoutStarted event) {
        List<UUID> articlesInCart = event.lineItems()
                .stream()
                .map(LineItem::getArticleId)
                .collect(Collectors.toList());
        Set<UUID> list = getForCustomer(event.customerId());
        list.removeAll(articlesInCart);
    }

    private Set<UUID> getForCustomer(UUID customerId) {
        return recommendedArticlesByCustomer.computeIfAbsent(customerId,
                id -> new HashSet<>());
    }

    public Set<UUID> getRecommendationsForCustomer(UUID customerId) {
        return Sets.newCopyOnWriteArraySet(getForCustomer(customerId));
    }

}
