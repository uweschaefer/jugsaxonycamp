package camp.jugsaxony.es.model.repo;

import java.util.UUID;

import camp.jugsaxony.es.model.cart.Cart;

public interface CartRepository {

    Cart findByCartId(UUID cartId);
}
