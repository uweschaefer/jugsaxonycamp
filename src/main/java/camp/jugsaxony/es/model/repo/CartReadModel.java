package camp.jugsaxony.es.model.repo;

import java.util.UUID;

import org.factcast.core.FactCast;
import org.factcast.core.spec.FactSpec;

import camp.jugsaxony.es.event.PullReadModel;
import camp.jugsaxony.es.event.cart.CheckoutStarted;
import camp.jugsaxony.es.event.cart.LineItemAdded;
import camp.jugsaxony.es.event.cart.LineItemQuantityChanged;
import camp.jugsaxony.es.event.cart.LineItemRemoved;
import camp.jugsaxony.es.model.cart.Cart;
import camp.jugsaxony.es.model.cart.LineItem;
import lombok.Getter;

public class CartReadModel extends PullReadModel {

    @Getter
    private Cart cart;

    CartReadModel(FactCast eventStore, UUID cartId) {
        super(eventStore, () -> FactSpec.ns("jugsaxony").aggId(cartId));
        cart = new Cart(cartId);
    }

    // Event handling Methods:

    public void apply(LineItemAdded event) {
        cart.setCustomerId(event.customerId());
        LineItem li = new LineItem(event.articleId());
        li.setQuantity(event.quantity());
        cart.getLineItems().put(li.getArticleId(), li);
        cart.setCustomerId(event.customerId());
    }

    public void apply(LineItemRemoved event) {
        cart.setCustomerId(event.customerId());
        cart.getLineItems().remove(event.articleId());
    }

    public void apply(LineItemQuantityChanged event) {
        cart.setCustomerId(event.customerId());
        cart.getLineItems().get(event.articleId()).setQuantity(event.quantity());
    }

    public void apply(CheckoutStarted event) {
        cart.setCustomerId(event.customerId());
        cart.setClosed(true);
    }
}
