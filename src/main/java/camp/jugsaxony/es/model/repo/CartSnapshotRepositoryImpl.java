package camp.jugsaxony.es.model.repo;

import java.util.Map;
import java.util.UUID;

import org.factcast.core.FactCast;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import camp.jugsaxony.es.model.cart.Cart;
import camp.jugsaxony.es.util.LRUMap;
import lombok.RequiredArgsConstructor;

@Primary
@Service
@RequiredArgsConstructor
public class CartSnapshotRepositoryImpl implements CartRepository {

    final FactCast eventStore;

    final Map<UUID, CartReadModel> modelCache = new LRUMap<>();

    @Override
    public Cart findByCartId(UUID cartId) {
        CartReadModel readModel = modelCache.computeIfAbsent(cartId, id -> new CartReadModel(
                eventStore, id));
        readModel.pull();
        return readModel.getCart();
    }

}
