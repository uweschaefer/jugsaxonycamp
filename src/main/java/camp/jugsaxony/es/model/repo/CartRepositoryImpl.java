package camp.jugsaxony.es.model.repo;

import java.util.UUID;

import org.factcast.core.FactCast;
import org.springframework.stereotype.Service;

import camp.jugsaxony.es.model.cart.Cart;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CartRepositoryImpl implements CartRepository {

    final FactCast eventStore;

    @Override
    public Cart findByCartId(UUID cartId) {
        CartReadModel readModel = new CartReadModel(eventStore, cartId);
        readModel.pull();
        return readModel.getCart();
    }

}
