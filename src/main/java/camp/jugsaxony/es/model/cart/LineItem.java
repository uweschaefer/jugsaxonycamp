package camp.jugsaxony.es.model.cart;

import java.util.UUID;

import lombok.Data;

@Data
public class LineItem {
    protected LineItem() {
    }

    public LineItem(UUID articleId) {
        this(articleId, 1);
    }

    public LineItem(UUID articleId, int quantity) {
        this.articleId = articleId;
        this.quantity = quantity;
    }

    UUID articleId;

    int quantity = 1;
}
