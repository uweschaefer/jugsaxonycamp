package camp.jugsaxony.es.model.cart;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import lombok.Data;

@Data
public class Cart {

    UUID customerId;

    final UUID cartId;

    Map<UUID, LineItem> lineItems = new HashMap<>();

    boolean closed = false;

    public void setCustomerId(UUID id) {
        if (customerId != null && !customerId.equals(id)) {
            throw new IllegalArgumentException("CustomerId Change not allowed");
        } else {
            customerId = id;
        }

    }

    public int calculateOverallQuantity() {
        return lineItems.values().stream().mapToInt(LineItem::getQuantity).sum();
    }

}
