package camp.jugsaxony.es.util;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.factcast.core.DefaultFact;
import org.factcast.core.Fact;
import org.factcast.core.util.FactCastJson;

import com.google.common.collect.Sets;

import camp.jugsaxony.es.event.AggregateId;
import camp.jugsaxony.es.event.Event;
import camp.jugsaxony.es.event.AggregateId.Missing;
import lombok.SneakyThrows;

public class Util {
    public static Fact toFact(Event e) {

        String payload = FactCastJson.writeValueAsString(e);
        String header = createHeader(UUID.randomUUID(), e);
        return DefaultFact.of(header, payload);
    }

    private static String createHeader(UUID id, Event e) {
        Map<String, Object> header = new HashMap<>();
        header.put("id", id);
        header.put("ns", "jugsaxony");
        header.put("type", e.getClass().getCanonicalName());
        header.put("aggIds", Sets.newHashSet(fetchAggregateId(e)));
        return FactCastJson.writeValueAsString(header);
    }

    private static UUID fetchAggregateId(Event e) {
        return Arrays.stream(e.getClass().getDeclaredFields())
                .filter(f -> f.getAnnotation(AggregateId.class) != null)
                .findFirst()
                .map(f -> fetchUUIDFromField(e, f))
                .orElseThrow(AggregateId.Missing::new);

    }

    @SneakyThrows
    private static UUID fetchUUIDFromField(Event e, Field f) {
        f.setAccessible(true);
        return (UUID) f.get(e);
    }

    @SneakyThrows
    public static <T extends Event> T toEvent(Fact f) {
        Class<?> eventClass = Class.forName(f.type());
        return (T) FactCastJson.readValue(eventClass, f.jsonPayload());

    }

}
