package camp.jugsaxony.es;

import org.factcast.core.FactCast;
import org.factcast.core.store.FactStore;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class EsApplication {

    public static void main(String[] args) {
        SpringApplication.run(EsApplication.class, args);
    }

    @Bean
    public FactCast factCast(FactStore store) {
        return FactCast.from(store);
    }
}
