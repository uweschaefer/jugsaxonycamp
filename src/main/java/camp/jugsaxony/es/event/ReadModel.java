package camp.jugsaxony.es.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.factcast.core.Fact;
import org.factcast.core.FactCast;
import org.factcast.core.spec.FactSpec;
import org.factcast.core.subscription.observer.FactObserver;

import camp.jugsaxony.es.util.Util;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class ReadModel implements FactObserver {

    private final Map<Class<?>, Consumer<Event>> eventHandlers = new HashMap<>();

    protected UUID lastFactConsumed = null;

    protected List<FactSpec> factsOfInterest;

    protected FactCast eventStore;

    public ReadModel(FactCast eventStore, Supplier<FactSpec> spec) {
        this.eventStore = eventStore;
        Collection<Method> methods = Arrays.asList(this.getClass().getMethods());
        methods.stream().filter(isHandleMethod()).forEach(m -> {
            Class<?> type = m.getParameterTypes()[0];
            eventHandlers.put(type, e -> {
                try {
                    m.invoke(this, e);
                } catch (IllegalAccessException | IllegalArgumentException
                        | InvocationTargetException e1) {
                    log.error("Reflective invocation failed", e1);
                }
            });
        });
        factsOfInterest = eventHandlers.keySet()
                .stream()
                .map(c -> c.getCanonicalName())
                .map(t -> spec.get().type(t))
                .collect(Collectors.toList());
    }

    private Predicate<? super Method> isHandleMethod() {
        return m -> (m.getName().startsWith("apply")) && (m
                .getParameterTypes().length == 1) && (Event.class.isAssignableFrom(m
                        .getParameterTypes()[0])) && (m.getReturnType().equals(void.class));
    }

    @Override
    @SneakyThrows
    public void onNext(Fact f) {
        log.info("consuming " + f.type() + " " + f.id());

        Event e = Util.toEvent(f);
        eventHandlers.get(Class.forName(f.type())).accept(e);
        lastFactConsumed = f.id();
    }

}
