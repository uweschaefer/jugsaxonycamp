package camp.jugsaxony.es.event.cart;

import java.util.Collection;
import java.util.UUID;

import camp.jugsaxony.es.event.AggregateId;
import camp.jugsaxony.es.event.Event;
import camp.jugsaxony.es.model.cart.Cart;
import camp.jugsaxony.es.model.cart.LineItem;
import lombok.Data;

@Data
public class CheckoutStarted implements Event {
    @AggregateId
    UUID cartId;

    UUID customerId;

    Collection<LineItem> lineItems;

    int overallQuantity;

    public static CheckoutStarted from(Cart cart) {
        return new CheckoutStarted()
                .cartId(cart.getCartId())
                .lineItems(cart.getLineItems().values())
                .customerId(cart.getCustomerId())
                .overallQuantity(cart.calculateOverallQuantity());

    }

}
