package camp.jugsaxony.es.event.cart;

import java.util.UUID;

import camp.jugsaxony.es.event.AggregateId;
import camp.jugsaxony.es.event.Event;
import lombok.Data;

@Data
public class LineItemRemoved implements Event {

    @AggregateId
    UUID cartId;

    UUID customerId;

    UUID articleId;
}
