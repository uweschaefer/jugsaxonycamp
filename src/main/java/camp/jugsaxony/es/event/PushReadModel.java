package camp.jugsaxony.es.event;

import java.util.function.Supplier;

import org.factcast.core.FactCast;
import org.factcast.core.spec.FactSpec;
import org.factcast.core.subscription.SubscriptionRequest;
import org.springframework.beans.factory.SmartInitializingSingleton;

public class PushReadModel extends ReadModel implements SmartInitializingSingleton {

    public PushReadModel(FactCast eventStore, Supplier<FactSpec> spec) {
        super(eventStore, spec);
    }

    @Override
    public void afterSingletonsInstantiated() {
        eventStore.subscribeToFacts(
                SubscriptionRequest.follow(factsOfInterest).fromScratch(), this);
    }
}
