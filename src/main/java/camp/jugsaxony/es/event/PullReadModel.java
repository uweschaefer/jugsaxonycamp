package camp.jugsaxony.es.event;

import java.util.function.Supplier;

import org.factcast.core.FactCast;
import org.factcast.core.spec.FactSpec;
import org.factcast.core.subscription.SubscriptionRequest;

public class PullReadModel extends ReadModel {

    public PullReadModel(FactCast eventStore, Supplier<FactSpec> spec) {
        super(eventStore, spec);
    }

    private void pullFromScratch() {
        eventStore.subscribeToFacts(
                SubscriptionRequest.catchup(factsOfInterest).fromScratch(),
                this)
                .awaitComplete();
    }

    public void pull() {
        if (lastFactConsumed == null) {
            pullFromScratch();
        } else {
            eventStore.subscribeToFacts(
                    SubscriptionRequest.catchup(factsOfInterest).from(lastFactConsumed), this)
                    .awaitComplete();
        }
    }

}
