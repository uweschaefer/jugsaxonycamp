package camp.jugsaxony.es.service;

import java.util.UUID;

import org.factcast.core.FactCast;
import org.springframework.stereotype.Service;

import camp.jugsaxony.es.event.cart.CheckoutStarted;
import camp.jugsaxony.es.event.cart.LineItemAdded;
import camp.jugsaxony.es.event.cart.LineItemQuantityChanged;
import camp.jugsaxony.es.event.cart.LineItemRemoved;
import camp.jugsaxony.es.model.cart.Cart;
import camp.jugsaxony.es.model.cart.LineItem;
import camp.jugsaxony.es.model.repo.CartRepository;
import camp.jugsaxony.es.util.Util;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CartService {
    final FactCast eventStore;

    final CartRepository cartRepo;

    public void addToCart(UUID cartId, UUID articleId, UUID customerId, int quantity) {
        // acquire necessary locks

        Cart currentCart = cartRepo.findByCartId(cartId);

        if (currentCart.getLineItems().containsKey(articleId)) {
            // increaseQuantity of the corresponding LineItem;
            LineItem lineItem = currentCart.getLineItems().get(articleId);
            LineItemQuantityChanged event = new LineItemQuantityChanged()
                    .articleId(articleId)
                    .cartId(cartId)
                    .customerId(customerId)
                    .quantity(lineItem.getQuantity() + quantity);

            eventStore.publish(Util.toFact(event));
        } else {
            // add new LineItem
            LineItemAdded event = new LineItemAdded()
                    .articleId(articleId)
                    .cartId(cartId)
                    .customerId(customerId)
                    .quantity(quantity);

            eventStore.publish(Util.toFact(event));
        }
        // release locks

    }

    public void checkout(UUID cartId) {
        Cart cart = cartRepo.findByCartId(cartId);
        if (cart.getLineItems().isEmpty()) {
            throw new IllegalStateException("Cart is Empty!");
        } else {
            eventStore.publish(Util.toFact(CheckoutStarted.from(cart)));
        }
    }

    public void removeFromCart(UUID cartId, UUID articleId, UUID customerId) {
        eventStore.publish(Util.toFact(new LineItemRemoved()
                .articleId(articleId)
                .cartId(cartId)
                .customerId(customerId)));
    }
}
